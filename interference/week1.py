#!/usr/bin/env python3
import sys, os
import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

def parse_filename(filename):
    match = re.match(r"^([0-9.]+)mm_([0-9.]+)mm.*$", os.path.basename(filename))
    return float(match.group(1)) * .5 * 1e-3, float(match.group(2)) * .5 * 1e-3

def read_oscilloscope(filename):
    df = pd.read_csv(filename, sep='\t', skiprows=7)
    df.iloc[:, 2] = df.iloc[:, 2].apply(np.abs)
    return df.iloc[:, 3], df.iloc[:, 2]

def fit_fraunhofer(theta, intensity, slit):
    k = 2. * np.pi / 632.8e-9
    proto = lambda theta, alpha, beta, gamma, delta: delta + alpha * (np.sin(beta * theta + gamma) / (beta * theta + gamma)) ** 2.
    fit = scipy.optimize.curve_fit(proto, theta, intensity, bounds=((6., .9 * k * slit, -10., -2.), (12., 1.1 * k * slit, 10., 2.)))
    return proto, fit[0]

if __name__ == "__main__":
    voltage_theta_slope = (1.655 - (-1.638)) / (np.pi / 6)
    theta, intensity = read_oscilloscope(sys.argv[1])
    theta /= voltage_theta_slope
    slit, extra_slit = parse_filename(sys.argv[1])

    proto, (alpha, beta, gamma, delta) = fit_fraunhofer(theta, intensity, slit)
    f = lambda t: proto(t, alpha, beta, gamma, delta)
    print(alpha, beta, gamma, delta)
    plt.scatter(theta, intensity, color="red")
    plt.plot(theta, f(theta))
    plt.show()
