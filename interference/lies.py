#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 16

k = 2. * np.pi / 632.8e-9
VOLTS_PER_RAD = 4.507 # V

if __name__ == "__main__":
    w = np.array([.01, .02, .04, .08]) # mm
    beta = np.deg2rad(np.array([28.013, 52.659, 99.883, 193.977])) # 1 / deg

    proto = lambda w_, a, b: a / VOLTS_PER_RAD * w_ + b
    popt, pcov = scipy.optimize.curve_fit(proto, w, beta)
    print(popt)

    plt.scatter(w, beta, color="steelblue")
    plt.errorbar(w, proto(w, *popt), .087, 0., color="red")

    plt.title(r"$\alpha_3$ for various slit widths")
    plt.xlabel(r"Slit width $\left[\mathrm{mm}\right]$")
    plt.ylabel(r"$\alpha_3\;\left[{{}^\circ}^{-1}\right]$")
    plt.grid()
    plt.show()
