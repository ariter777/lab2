#!/usr/bin/env python3
import sys, os
import argparse
import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize, scipy.stats

plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 16
k = 2. * np.pi / 632.8e-9
extra_slit = .05e-3 # 0.05mm far slit
VOLTS_PER_RAD = 4.507 # V

def parse_filename(filename):
    match = re.match(r"^([0-9.]+)mm(?:_([0-9.]+)mm)?_([0-9]).*$", os.path.basename(filename)) # width_distance_nslits
    return float(match.group(1)) * .5 * 1e-3, float(match.group(2)) * .5 * 1e-3 if match.group(2) is not None else None, int(match.group(3))

def read_oscilloscope(filename):
    with open(filename, "r") as rfd:
        rfd.readline()
        line = rfd.readline()
    sr = float(re.match(r"^Sample Rate \(Hz\): ([0-9.]+)$", line).group(1))
    df = pd.read_csv(filename, sep='\t', skiprows=7)
    return sr, df

def get_bad_freqs(filename, n_freqs=2):
    sr, df = read_oscilloscope(filename)

    freqs = np.fft.fftfreq(df.shape[0], d=1. / sr)
    fourier = np.fft.fft(df["Ch1[V]"])

    freqs_idx_sorted = np.argsort(np.abs(fourier))
    freqs_idx_sorted = freqs_idx_sorted[np.abs(freqs[freqs_idx_sorted]) > 10.] # Only look for bad frequencies above 10Hz
    bad_freqs_idx = freqs_idx_sorted[::-1][0:2 * n_freqs] # both plus and minus; no need for 0 Hz
    return freqs[bad_freqs_idx]

def clean_bad_freqs(df, sr, bad_freqs, channel="Ch1[V]", plot=False):
    freqs = np.fft.fftfreq(df.shape[0], d=1. / sr)
    fourier = np.fft.fft(df["Ch1[V]"])
    bad_freqs_idx = [np.argmin(np.abs(freqs - bad_freq)) for bad_freq in bad_freqs]

    if plot:
        plt.scatter(freqs, np.abs(fourier))
        plt.xlabel(r"Frequency $\left[\mathrm{Hz}\right]$")
        plt.ylabel(r"Voltage $\left[\mathrm{V}\right]$")
        plt.title("Fourier transform of voltage over time for fixed angle")
        plt.show()

    median = np.median(np.abs(fourier))
    for idx in bad_freqs_idx:
        fourier[idx - 50:idx + 51] = median # the Fourier amplitude in the clean frequencies isn't precisely 0

    df_clean = df.copy()
    df_clean[channel] = np.real(np.fft.ifft(fourier))
    return df_clean

def fit_fraunhofer(voltage, intensity, n_slits=1):
    proto = lambda voltage, alpha, beta, gamma, delta, epsilon: delta + alpha * (np.sin(beta * (voltage + gamma)) / (beta * (voltage + gamma)) *
                                                                                 np.sin(n_slits * epsilon * (voltage + gamma)) / np.sin(epsilon * (voltage + gamma))) ** 2.
    fit = scipy.optimize.curve_fit(proto, voltage, intensity, bounds=((0., 30., -.06, 0., 300.), (4., 60., -.02, .2, 400.)))
    return proto, fit

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data", help="The specific measurement (probably per slit combination, etc.)")
    parser.add_argument("--baseline-voltage", help="Oscilloscope recording of voltage as the system stands still (for noisy frequency cleanup).")
    parser.add_argument("--freqs", help="How many of the noisiest frequencies should be cleaned using the --baseline-voltage file.", type=int, default=2)
    parser.add_argument("--plot", help="Whether to display plots or only save them.", action="store_true")
    args = parser.parse_args()

    sr, df = read_oscilloscope(args.data)

    if args.baseline_voltage: # Perform frequency cleanup
        bad_freqs = get_bad_freqs(args.baseline_voltage, args.freqs)
        sys.stderr.write(f"Killing frequencies: {bad_freqs} Hz\n")
        df = clean_bad_freqs(df, sr, bad_freqs, plot=False)

    width, dist, n_slits = parse_filename(args.data)

    proto, ((alpha, beta, gamma, delta, epsilon), pcov) = fit_fraunhofer(df["Ch1[V]"], -df["Ch0[V]"], n_slits)
    proto = lambda voltage, alpha, beta, gamma, delta, epsilon: delta + alpha * (np.sin(beta * (voltage + gamma)) / (beta * (voltage + gamma)) *
                                                                                 np.sin(n_slits * epsilon * (voltage + gamma)) / np.sin(epsilon * (voltage + gamma))) ** 2.

    calc_dist = VOLTS_PER_RAD * epsilon / k
    calc_width = VOLTS_PER_RAD * beta / k
    print(alpha, beta, gamma, delta, epsilon)
    print(f"Slit width{'s' if n_slits > 1 else ''}: {round(calc_width / 1e-3, 6)}mm vs actual {width / 1e-3:.5f}mm")
    if n_slits > 1:
        print(f"Slit distance: {round(calc_dist / 1e-3, 6)}mm vs actual {dist / 1e-3:.5f}mm")
    f = lambda t: proto(t, alpha, beta, gamma, delta, epsilon)
    r2 = scipy.stats.pearsonr(-df["Ch0[V]"], f(df["Ch1[V]"]))[0] ** 2.
    print(f"""Pearson R^2: {r2}""")

    plt.scatter(np.rad2deg(df["Ch1[V]"] / VOLTS_PER_RAD), -df["Ch0[V]"], color="red", label="Measurement")
    plt.plot(np.rad2deg(df["Ch1[V]"] / VOLTS_PER_RAD), f(df["Ch1[V]"]), label=rf"Fit ($r^2 = {r2 * 100:.3f}\%$)")
    plt.title((rf"Calculated slit width{'s' if n_slits > 1 else ''}: $\mathrm{{{round(calc_width / 1e-3, 3)}mm}}$ vs actual $\mathrm{{{width / 1e-3}mm}}$" + '\n') +
              ((rf"Calculated distance {'between' if n_slits == 2 else 'among'} {n_slits} slits: $\mathrm{{{round(calc_dist / 1e-3, 3)}mm}}$ vs actual $\mathrm{{{dist / 1e-3}mm}}$" + '\n') if n_slits > 1 else "") +
              ("Frequency cleanup performed" if args.baseline_voltage and args.freqs > 0 else "No  frequency cleanup"))
    plt.xlabel(r"$\theta\;\left[{}^\circ\right]$")
    plt.ylabel(r"$I\;\left[\mathrm{W}\cdot\mathrm{m}^{-2}\right]$")
    plt.grid()
    plt.legend()
    plt.gcf().set_size_inches(16, 8)
    plt.gcf().set_dpi(112)

    plt.savefig(os.path.join("results", os.path.splitext(os.path.relpath(args.data, "data"))[0] + ".png"))
    if args.plot:
        plt.show()
