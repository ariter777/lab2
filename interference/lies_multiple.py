#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 16

k = 2. * np.pi / 632.8e-9
VOLTS_PER_RAD = 4.507 # V
DIST = .0125 # mm

if __name__ == "__main__":
    n = np.arange(2, 6)
    epsilon = np.deg2rad(np.array([298.0694, 441.1323, 588.3402, 734.5305])) # 1 / deg

    proto = lambda w_, a, b: a * .5 * DIST / VOLTS_PER_RAD * w_ + b
    popt, pcov = scipy.optimize.curve_fit(proto, n, epsilon)
    print(popt)

    plt.scatter(n, epsilon, color="steelblue")
    plt.errorbar(n, proto(n, *popt), .35, 0., color="red")
    plt.xticks(n)

    plt.title(r"$\alpha_5$ for various numbers of slits")
    plt.xlabel(r"Number of slits")
    plt.ylabel(r"$\alpha_5\;\left[{{}^\circ}^{-1}\right]$")
    plt.grid()
    plt.show()
